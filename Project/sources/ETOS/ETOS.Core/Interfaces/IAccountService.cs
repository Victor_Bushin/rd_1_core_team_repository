﻿using ETOS.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETOS.Core.Interfaces
{
    interface IAccountService : IService<Accounts>
    {
        bool Autorization(string login, string password);
    }
}
