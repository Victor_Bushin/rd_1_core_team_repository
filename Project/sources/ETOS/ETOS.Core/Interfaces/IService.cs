﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETOS.Core.Interfaces
{
    interface IService<TEntity>
    {
        bool Add(TEntity item);
        bool Delete(TEntity item);
        bool Edit(TEntity item);
        bool Get(TEntity item);
    }
}
