﻿using ETOS.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ETOS.DAL;
using ETOS.DAL.Interfaces;

namespace ETOS.Core.Classes
{
    class AccountService : IAccountService
    {
        IAccountRepository AccountRepository;

        AccountService(IAccountRepository accountRepository)
        {
            AccountRepository = accountRepository;
        }
        public bool Add(Accounts item)
        {
            throw new NotImplementedException();
        }

        public bool Autorization(string login, string password)
        {
            return AccountRepository.Find(acc => (acc.Password == password && acc.Login == login)).Any();
        }

        public bool Delete(Accounts item)
        {
            throw new NotImplementedException();
        }

        public bool Edit(Accounts item)
        {
            throw new NotImplementedException();
        }

        public bool Get(Accounts item)
        {
            throw new NotImplementedException();
        }
    }
}
