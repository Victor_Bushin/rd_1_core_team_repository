namespace ETOS.DAL
{
    using DAL.Interfaces;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class Accounts : IEntity
    {

        public object ident
        {
            get
            {
                return this;
            }

             set { }
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int AccountId { get; set; }

        public int EmployeeId { get; set; }

        [Required]
        [StringLength(16)]
        public string Login { get; set; }

        [Required]
        [StringLength(20)]
        public string Password { get; set; }

        public virtual Employees Employees { get; set; }
    }
}
