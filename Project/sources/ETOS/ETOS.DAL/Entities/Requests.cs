namespace ETOS.DAL
{
    using DAL.Interfaces;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class Requests : IEntity
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Requests()
        {
            Routes = new HashSet<Routes>();
            RqstsLifeCycles = new HashSet<RqstsLifeCycles>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int RequestId { get; set; }

        public int AuthorId { get; set; }

        public int? DeparturePoint { get; set; }

        public int? DestinationPoint { get; set; }

        [Required]
        [StringLength(255)]
        public string DepartureAddress { get; set; }

        [Required]
        [StringLength(255)]
        public string DestinationAddress { get; set; }

        public DateTime DepartureDateTime { get; set; }

        public DateTime CreatingDateTime { get; set; }

        public bool IsBaggage { get; set; }

        [StringLength(255)]
        public string Comment { get; set; }

        public double Mileage { get; set; }

        public double Price { get; set; }

        public virtual Employees Employees { get; set; }

        public virtual Locations Locations { get; set; }

        public virtual Locations Locations1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Routes> Routes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RqstsLifeCycles> RqstsLifeCycles { get; set; }
    }
}
