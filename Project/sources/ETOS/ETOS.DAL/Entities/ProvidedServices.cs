namespace ETOS.DAL
{
    using DAL.Interfaces;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class ProvidedServices : IEntity
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ContractorId { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int TransportTypeId { get; set; }

        public int MinimalNumberOfSeats { get; set; }

        public int MaximalNumberOfSeats { get; set; }

        public double Price { get; set; }
    }
}
