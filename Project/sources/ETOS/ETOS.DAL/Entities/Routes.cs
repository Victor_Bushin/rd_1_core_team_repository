namespace ETOS.DAL
{
    using DAL.Interfaces;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class Routes : IEntity
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Routes()
        {
            Routes1 = new HashSet<Routes>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int RoutePointId { get; set; }

        public int RequestId { get; set; }

        public int DestinationPointId { get; set; }

        [Required]
        [StringLength(255)]
        public string DestinationAddress { get; set; }

        public int NextRoutePointId { get; set; }

        public virtual Locations Locations { get; set; }

        public virtual Requests Requests { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Routes> Routes1 { get; set; }

        public virtual Routes Routes2 { get; set; }
    }
}
