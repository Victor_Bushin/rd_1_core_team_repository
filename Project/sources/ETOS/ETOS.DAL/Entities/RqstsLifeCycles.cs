namespace ETOS.DAL
{
    using DAL.Interfaces;
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class RqstsLifeCycles : IEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int EventId { get; set; }

        public int RequestId { get; set; }

        public DateTime EventDateTime { get; set; }

        public int StatusId { get; set; }

        public int AuthorId { get; set; }

        public virtual Employees Employees { get; set; }

        public virtual Requests Requests { get; set; }

        public virtual Statuses Statuses { get; set; }
    }
}
