namespace ETOS.DAL
{
    using DAL.Interfaces;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class TransportTypes : IEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int TransportTypeId { get; set; }

        [Required]
        [StringLength(30)]
        public string Name { get; set; }
    }
}
