namespace ETOS.DAL
{
    using DAL.Interfaces;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class Limits : IEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int LimitId { get; set; }

        public int? EmployeeId { get; set; }

        public double? FullPriceLimit { get; set; }

        public double? TripCountLimit { get; set; }

        public virtual Employees Employees { get; set; }
    }
}
