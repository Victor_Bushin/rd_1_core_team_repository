namespace ETOS.DAL
{
    using DAL.Interfaces;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class Positions : IEntity
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Positions()
        {
            Employees = new HashSet<Employees>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PositionId { get; set; }

        [Required]
        [StringLength(100)]
        public string PositionName { get; set; }

        public int PriorityId { get; set; }

        public bool IsManager { get; set; }

        public int TransportTypeId { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Employees> Employees { get; set; }

        public virtual Priorities Priorities { get; set; }
    }
}
