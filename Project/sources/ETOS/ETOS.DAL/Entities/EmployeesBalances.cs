namespace ETOS.DAL
{
    using DAL.Interfaces;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class EmployeesBalances : IEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int EmployeeId { get; set; }

        public int TripsBalance { get; set; }

        public int MoneyBalance { get; set; }

        public virtual Employees Employees { get; set; }
    }
}
