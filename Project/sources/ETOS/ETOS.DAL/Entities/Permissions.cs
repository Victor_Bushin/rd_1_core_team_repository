namespace ETOS.DAL
{
    using DAL.Interfaces;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class Permissions : IEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PermissionId { get; set; }

        public bool IsCommonUser { get; set; }

        public bool IsSecretary { get; set; }

        public bool IsAccountant { get; set; }

        public bool IsManager { get; set; }

        public bool IsAdministrator { get; set; }

        public int EmployeeId { get; set; }

        public virtual Employees Employees { get; set; }
    }
}
