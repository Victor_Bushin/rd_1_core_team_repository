namespace ETOS.DAL
{
    using DAL.Interfaces;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class Orgstructures : IEntity
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Orgstructures()
        {
            Employees = new HashSet<Employees>();
            Orgstructures1 = new HashSet<Orgstructures>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int OrgstructureId { get; set; }

        [Required]
        [StringLength(150)]
        public string Name { get; set; }

        public int? ParentOrgstructureId { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Employees> Employees { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Orgstructures> Orgstructures1 { get; set; }

        public virtual Orgstructures Orgstructures2 { get; set; }
    }
}
