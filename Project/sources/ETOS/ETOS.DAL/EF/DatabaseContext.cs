﻿using System;
using System.Data.Entity;
using ETOS.DAL.Interfaces;


namespace ETOS.DAL.EF
{
    /// <summary>
    /// Provides connection to DataBase
    /// </summary>
    public class DatabaseContext : DbContext, IEntity
    {
        /// <summary>
        /// Accounts table
        /// </summary>
        public DbSet<Accounts> Account { get; set; }

        /// <summary>
        /// Employees table
        /// </summary>
        public DbSet<Employees> Employee { get; set; }

        /// <summary>
        /// Constructor for DataBaseContext
        /// Initialize some entities using test values
        /// </summary>
        public DatabaseContext() 
            //: base("name=DatabaseContext")  // добавить connectionString в App config
        {            
            Database.SetInitializer<DatabaseContext>(new StoreDbInitializer());
        }
               

        /// <summary>
        /// Creates new context object using string for connection to DataBase
        /// </summary>
        /// <param name="connectionString"></param>
        public DatabaseContext(string connectionString) : base(connectionString)
        {
        }
    }




    public class StoreDbInitializer : DropCreateDatabaseIfModelChanges<DatabaseContext>
    {
        protected override void Seed(DatabaseContext db)
        {
            db.Account.Add(new Accounts { AccountId = 1, EmployeeId = 1, Login = "Vasya", Password = "12345" });
            db.Account.Add(new Accounts { AccountId = 2, EmployeeId = 1, Login = "Vova", Password = "123" });
            db.Employee.Add(new Employees { EmployeeId = 1, FirstName = "Vasily", MiddleName = "Popov", LastName = "Ivanovich", Email = "V@m.ru", Phone = "89233456594", LocationId = 1, OrgstructureId = 1, PositionId = 1 });
            db.Employee.Add(new Employees { EmployeeId = 1, FirstName = "Vladimir", MiddleName = "Terekov", LastName = "Kuzmich", Email = "k@m.ru", Phone = "89233849394", LocationId = 1, OrgstructureId = 1, PositionId = 1 });
            db.SaveChanges();
        }
    }

}
