namespace ETOS.DAL
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class DataBaseModel : DbContext
    {
        public DataBaseModel()
            : base("name=DataBaseModel")
        {
        }

        public virtual DbSet<Accounts> Accounts { get; set; }
        public virtual DbSet<Contractors> Contractors { get; set; }
        public virtual DbSet<Employees> Employees { get; set; }
        public virtual DbSet<EmployeesBalances> EmployeesBalances { get; set; }
        public virtual DbSet<Limits> Limits { get; set; }
        public virtual DbSet<Locations> Locations { get; set; }
        public virtual DbSet<Orgstructures> Orgstructures { get; set; }
        public virtual DbSet<Permissions> Permissions { get; set; }
        public virtual DbSet<Positions> Positions { get; set; }
        public virtual DbSet<Priorities> Priorities { get; set; }
        public virtual DbSet<ProvidedServices> ProvidedServices { get; set; }
        public virtual DbSet<Requests> Requests { get; set; }
        public virtual DbSet<Routes> Routes { get; set; }
        public virtual DbSet<RqstsLifeCycles> RqstsLifeCycles { get; set; }
        public virtual DbSet<Services> Services { get; set; }
        public virtual DbSet<Statuses> Statuses { get; set; }
        public virtual DbSet<TransportTypes> TransportTypes { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Accounts>()
                .Property(e => e.Login)
                .IsFixedLength();

            modelBuilder.Entity<Accounts>()
                .Property(e => e.Password)
                .IsFixedLength();

            modelBuilder.Entity<Contractors>()
                .Property(e => e.Name)
                .IsFixedLength();

            modelBuilder.Entity<Contractors>()
                .Property(e => e.Phone)
                .IsFixedLength();

            modelBuilder.Entity<Employees>()
                .Property(e => e.FirstName)
                .IsFixedLength();

            modelBuilder.Entity<Employees>()
                .Property(e => e.MiddleName)
                .IsFixedLength();

            modelBuilder.Entity<Employees>()
                .Property(e => e.LastName)
                .IsFixedLength();

            modelBuilder.Entity<Employees>()
                .Property(e => e.Phone)
                .IsFixedLength();

            modelBuilder.Entity<Employees>()
                .Property(e => e.Email)
                .IsFixedLength();

            modelBuilder.Entity<Employees>()
                .HasMany(e => e.Accounts)
                .WithRequired(e => e.Employees)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Employees>()
                .HasOptional(e => e.EmployeesBalances)
                .WithRequired(e => e.Employees);

            modelBuilder.Entity<Employees>()
                .HasMany(e => e.Permissions)
                .WithRequired(e => e.Employees)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Employees>()
                .HasMany(e => e.Requests)
                .WithRequired(e => e.Employees)
                .HasForeignKey(e => e.AuthorId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Employees>()
                .HasMany(e => e.RqstsLifeCycles)
                .WithRequired(e => e.Employees)
                .HasForeignKey(e => e.AuthorId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Locations>()
                .Property(e => e.Name)
                .IsFixedLength();

            modelBuilder.Entity<Locations>()
                .Property(e => e.Address)
                .IsFixedLength();

            modelBuilder.Entity<Locations>()
                .HasMany(e => e.Employees)
                .WithRequired(e => e.Locations)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Locations>()
                .HasMany(e => e.Locations1)
                .WithOptional(e => e.Locations2)
                .HasForeignKey(e => e.ParentLocationId);

            modelBuilder.Entity<Locations>()
                .HasMany(e => e.Requests)
                .WithOptional(e => e.Locations)
                .HasForeignKey(e => e.DeparturePoint);

            modelBuilder.Entity<Locations>()
                .HasMany(e => e.Requests1)
                .WithOptional(e => e.Locations1)
                .HasForeignKey(e => e.DestinationPoint);

            modelBuilder.Entity<Locations>()
                .HasMany(e => e.Routes)
                .WithRequired(e => e.Locations)
                .HasForeignKey(e => e.DestinationPointId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Orgstructures>()
                .Property(e => e.Name)
                .IsFixedLength();

            modelBuilder.Entity<Orgstructures>()
                .HasMany(e => e.Employees)
                .WithRequired(e => e.Orgstructures)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Orgstructures>()
                .HasMany(e => e.Orgstructures1)
                .WithOptional(e => e.Orgstructures2)
                .HasForeignKey(e => e.ParentOrgstructureId);

            modelBuilder.Entity<Positions>()
                .Property(e => e.PositionName)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Positions>()
                .HasMany(e => e.Employees)
                .WithRequired(e => e.Positions)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Priorities>()
                .Property(e => e.Name)
                .IsFixedLength();

            modelBuilder.Entity<Priorities>()
                .HasMany(e => e.Positions)
                .WithRequired(e => e.Priorities)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Requests>()
                .Property(e => e.DepartureAddress)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Requests>()
                .Property(e => e.DestinationAddress)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Requests>()
                .Property(e => e.Comment)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Requests>()
                .HasMany(e => e.Routes)
                .WithRequired(e => e.Requests)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Requests>()
                .HasMany(e => e.RqstsLifeCycles)
                .WithRequired(e => e.Requests)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Routes>()
                .Property(e => e.DestinationAddress)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Routes>()
                .HasMany(e => e.Routes1)
                .WithRequired(e => e.Routes2)
                .HasForeignKey(e => e.NextRoutePointId);

            modelBuilder.Entity<Statuses>()
                .Property(e => e.Name)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Statuses>()
                .HasMany(e => e.RqstsLifeCycles)
                .WithRequired(e => e.Statuses)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TransportTypes>()
                .Property(e => e.Name)
                .IsFixedLength();
        }
    }


    public class StoreDbInitializer : DropCreateDatabaseIfModelChanges<DataBaseModel>
    {
        protected override void Seed(DataBaseModel db)
        {
            db.Accounts.Add(new Accounts { AccountId = 1, EmployeeId = 1, Login = "Vasya", Password = "12345" });
            db.Accounts.Add(new Accounts { AccountId = 2, EmployeeId = 1, Login = "Vova", Password = "123" });
            db.Employees.Add(new Employees { EmployeeId = 1, FirstName = "Vasily", MiddleName = "Popov", LastName = "Ivanovich", Email = "V@m.ru", Phone = "89233456594", LocationId = 1, OrgstructureId = 1, PositionId = 1 });
            db.Employees.Add(new Employees { EmployeeId = 1, FirstName = "Vladimir", MiddleName = "Terekov", LastName = "Kuzmich", Email = "k@m.ru", Phone = "89233849394", LocationId = 1, OrgstructureId = 1, PositionId = 1 });
            db.SaveChanges();
        }
    }

}
