﻿using ETOS.DAL.EF;
using ETOS.DAL.Interfaces;
using ETOS.DAL;

namespace DAL.Repositories
{
    /// <summary>
    /// Class for Employee entity interaction
    /// Inherited from BaseRepository<> that provides CRUD operations for entities    
    /// </summary>
    internal class EmployeesRepository : BaseRepository<Employees>
    {
        /// <summary>
        /// repository constructor 
        /// </summary>
        /// <param name="context">current database context</param>
        public EmployeesRepository(DatabaseContext context) : base(context)
        {
        }

        /*private DatabaseContext db;

        public EmployeesRepository(DatabaseContext context)
        {
            this.db = context;
        }

        public void Create(Employee item)
        {
            db.Employees.Add(item);
        }

        public void Delete(int id)
        {
            Employee emp = db.Employees.Find(id);
            if (emp != null)
                db.Employees.Remove(emp);
        }

        public IEnumerable<Employee> Find(Func<Employee, bool> predicate)
        {
            return db.Employees.Where(predicate).ToList();
        }

        public Employee Get(int id)
        {
            return db.Employees.Find(id);
        }

        public IEnumerable<Employee> GetAll()
        {
            return db.Employees;
        }

        public void Update(Employee item)
        {
            db.Entry(item).State = EntityState.Modified;
        }*/
    }
}
