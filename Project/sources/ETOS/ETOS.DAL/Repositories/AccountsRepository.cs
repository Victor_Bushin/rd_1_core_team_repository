﻿using ETOS.DAL.Interfaces;
using ETOS.DAL.EF;


namespace ETOS.DAL.Repositories
{
    /// <summary>
    /// Class for Account entity interaction
    /// Inherited from BaseRepository<> that provides CRUD operations for entities    
    /// </summary>
    internal class AccountsRepository : BaseRepository<Accounts>
    {
        /// <summary>
        /// repository constructor 
        /// </summary>
        /// <param name="context">current database context</param>
        public AccountsRepository(DatabaseContext context) : base(context)
        {
        }

        /* private DatabaseContext db;

         public AccountsRepository(DatabaseContext context)
         {
             this.db = context;
         }

         public void Create(Account item)
         {
             db.Accounts.Add(item);
         }

         public void Delete(int id)
         {
             Account acc = db.Accounts.Find(id);
             if (acc != null)
                 db.Accounts.Remove(acc);
         }

         public IEnumerable<Account> Find(Func<Account, bool> predicate)
         {
             return db.Accounts.Where(predicate).ToList();
         }

         public Account Get(int id)
         {
             return db.Accounts.Find(id);
         }

         public IEnumerable<Account> GetAll()
         {
             return db.Accounts;
         }

         public void Update(Account item)
         {
             db.Entry(item).State = EntityState.Modified;
         }*/
    }
}
