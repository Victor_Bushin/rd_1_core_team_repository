﻿using System;


namespace ETOS.DAL.Interfaces
{
    /// <summary>
    /// Provides specific actions for Account Entity
    /// </summary>
    public interface IAccountRepository : IRepository<Accounts>
    {
       
    }
}
