﻿CREATE TABLE [dbo].[Requests]
(
	[RequestId]				INT			NOT NULL,
	[AuthorId]				INT			NOT NULL,
	[DeparturePoint]		INT			NULL,
	[DestinationPoint]		INT			NULL,
	[DepartureAddress]		CHAR (255)	NOT NULL,
	[DestinationAddress]	CHAR (255)	NOT NULL,
	[DepartureDateTime]		DATETIME	NOT NULL,
	[CreatingDateTime]		DATETIME	NOT NULL,
	[IsBaggage]				BIT			NOT NULL,
	[Comment]				CHAR (255)	NULL, 
    [Mileage]				FLOAT		NOT NULL, 
    [Price]					FLOAT		NOT NULL,
	CONSTRAINT PK_Requests_RequestId PRIMARY KEY (RequestId),
	CONSTRAINT FK_Requests_AuthorId FOREIGN KEY (AuthorId)
		REFERENCES [dbo].[Employees](EmployeeId),
	CONSTRAINT FK_Requests_DeparturePoint FOREIGN KEY (DeparturePoint)
		REFERENCES [dbo].[Locations](LocationId),
	CONSTRAINT FK_Requests_DestinationPoint FOREIGN KEY (DestinationPoint)
		REFERENCES [dbo].[Locations](LocationId),
)