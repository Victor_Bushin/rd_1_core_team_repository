﻿CREATE TABLE [dbo].[TransportTypes]
(
	[TransportTypeId]	INT			NOT NULL, 
    [Name]				NCHAR(30)	NOT NULL,
	CONSTRAINT PK_TransportType_TransportTypeId PRIMARY KEY (TransportTypeId),
)
