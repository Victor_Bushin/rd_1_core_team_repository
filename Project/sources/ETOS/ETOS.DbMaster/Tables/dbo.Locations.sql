﻿CREATE TABLE [dbo].[Locations]
(
	[LocationId]		INT			NOT NULL,  
    [Name]				NCHAR(150)	NOT NULL, 
    [Address]			NCHAR(255)	NOT NULL, 
    [ParentLocationId]	INT			NULL, 
    [IsPOI]				BIT			NOT NULL, 
    [Culture]			INT			NOT NULL
	CONSTRAINT PK_Locations_LocationId PRIMARY KEY (LocationId),
	CONSTRAINT FK_Locations_ParentLocationId FOREIGN KEY (ParentLocationId)
		REFERENCES [dbo].[Locations](LocationId),
)