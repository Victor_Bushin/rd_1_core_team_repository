﻿CREATE TABLE [dbo].[ProvidedServices]
(
	[ContractorId]			INT		NOT NULL,
	[TransportTypeId]		INT		NOT NULL,	
	[MinimalNumberOfSeats]	INT		NOT NULL,
	[MaximalNumberOfSeats]	INT		NOT NULL,
	[Price]					FLOAT	NOT NULL,
	CONSTRAINT PK_ProvidedServices_ContractorId_TransportTypeId PRIMARY KEY (ContractorId,TransportTypeId),
)