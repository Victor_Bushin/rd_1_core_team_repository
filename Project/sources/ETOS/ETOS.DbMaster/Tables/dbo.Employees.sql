﻿CREATE TABLE [dbo].[Employees] (
    [EmployeeId]     INT        NOT NULL,
    [FirstName]      NCHAR (50) NOT NULL,
    [MiddleName]     NCHAR (50) NULL,
    [LastName]       NCHAR (50) NOT NULL,
    [Phone]          NCHAR (16) NOT NULL,
    [Email]          NCHAR (50) NOT NULL UNIQUE,
    [LocationId]     INT        NOT NULL,
    [PositionId]     INT        NOT NULL,
    [OrgstructureId] INT        NOT NULL,  
	CONSTRAINT PK_Employees_EmployeeId PRIMARY KEY (EmployeeId),
	CONSTRAINT FK_Employees_LocationId FOREIGN KEY (LocationId)
		REFERENCES [dbo].[Locations](LocationId), 
	CONSTRAINT FK_Employees_PositionId FOREIGN KEY (PositionId)
		REFERENCES [dbo].[Positions](PositionId),
	CONSTRAINT FK_Employees_OrgstructureId FOREIGN KEY (OrgstructureId)
		REFERENCES [dbo].[Orgstructures](OrgstructureId),
);

