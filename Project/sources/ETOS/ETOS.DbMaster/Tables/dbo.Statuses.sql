﻿CREATE TABLE [dbo].[Statuses]
(
	[StatusId]	INT			NOT NULL,
	[Name]		CHAR (30)	NOT NULL,
	CONSTRAINT PK_Statuses_StatusId PRIMARY KEY (StatusId),
)