﻿CREATE TABLE [dbo].[Positions]
(
	[PositionId]		INT			NOT NULL,
	[PositionName]		CHAR(100)	NOT NULL,
	[PriorityId]		INT			NOT NULL,
	[IsManager]			BIT			NOT NULL,
	[TransportTypeId]	INT			NOT NULL,
	CONSTRAINT PK_Positions_PositionId PRIMARY KEY (PositionId),
	CONSTRAINT FK_Positions_PriorityId FOREIGN KEY (PriorityId)
		REFERENCES [dbo].[Priorities](PriorityId),
)