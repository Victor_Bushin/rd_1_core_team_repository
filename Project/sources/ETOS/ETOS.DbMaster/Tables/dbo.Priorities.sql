﻿CREATE TABLE [dbo].[Priorities]
(
	[PriorityId]	INT			NOT NULL, 
    [Name]			NCHAR(30)	NOT NULL,
	CONSTRAINT PK_Priorities_PriorityId PRIMARY KEY (PriorityId),
)