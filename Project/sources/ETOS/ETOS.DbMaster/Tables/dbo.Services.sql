﻿CREATE TABLE [dbo].[Services]
(
	[ContractorId]	INT	NOT NULL,
	[LocationId]	INT NOT NULL,
	CONSTRAINT PK_Services_ContractorId_LocationId PRIMARY KEY ([ContractorId], [LocationId]),
)