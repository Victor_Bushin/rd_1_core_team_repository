﻿CREATE TABLE [dbo].[Accounts] (
    [AccountId]  INT        NOT NULL,
    [EmployeeId] INT        NOT NULL,
    [Login]      NCHAR (16) NOT NULL,
    [Password]   NCHAR (20) NOT NULL,
	CONSTRAINT PK_Accounts_AccountId PRIMARY KEY (AccountId), 
    CONSTRAINT [FK_Accounts_To_Employees] FOREIGN KEY ([EmployeeId]) REFERENCES [Employees]([EmployeeId]),

   );
